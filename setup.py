"""Setuptools module for s3uploader installation"""
from os import path
from setuptools import find_packages, setup

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(name='s3uploader',
      version='0.1',
      description='Upload files to an Amazon S3 bucket',
      long_description=long_description,
      author='Michael Anckaert',
      author_email='michael.anckaert@sinax.be',
      url='https://bitbucket.org/michaelanckaert/s3uploader/',
      license='MIT',

      packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
      install_requires=['boto3'],

      entry_points={
          'console_scripts': [
              's3uploader=s3uploader.main:run',
          ],
      },

      keywords='aws s3 uploader amazon',
      classifiers=[
          'Development Status :: 3 - Alpha',

          'Environment :: Console',

          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',

          'License :: OSI Approved :: MIT License',

          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',

          'Programming Language :: Python :: 3.5',

          'Topic :: Utilities',
      ]
      )
